#
# Description: <Method description here>
#
require 'vra'
require 'json'
$evm.log(:info, "VRA->****************************************************************************************")
$evm.log(:info, "VRA->Starting VRA Get Blueprints")

vrahost = "https://"+ $evm.object['vra_host']

$evm.log(:info, "VRA->Vrahost is #{vrahost}")
$evm.log(:info,vrahost)
vra = Vra::Client.new(username: $evm.object['vra_username'],password: $evm.object['vra_userpassword'],tenant: $evm.object['vra_tenant'], base_url: vrahost , verify_ssl: false)
$evm.log(:info, "VRA->Got VRA Connection")

dialog_field = $evm.object

# sort_by: value / description / none
dialog_field["sort_by"] = "value"

# sort_order: ascending / descending
dialog_field["sort_order"] = "ascending"

# data_type: string / integer
dialog_field["data_type"] = "string"

# required: true / false
dialog_field["required"] = "true"
tmphash = Hash.new
vra.catalog.entitled_items.each { |bp| tmphash[bp.id]=bp.name}


#dialog_field["values"] = {1 => "one", 2 => "two", 10 => "ten", 50 => "fifty"}
dialog_field["values"] = tmphash
#dialog_field["default_value"] = 2
$evm.log(:info, "VRA->Exiting VRA Get Blueprints")



