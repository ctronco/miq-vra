#
# Description: This method updates the provision status.
# Required inputs: status
#
$evm.log(:info, "VRA->Starting update_provision_status")

#prov = $evm.root['miq_provision']
prov = $evm.root['service_template_provision_task']
unless prov
  $evm.log(:error, "VRA->miq_provision object not provided")
  exit(MIQ_STOP)
end


num_retries =  $evm.root['ae_state_retries'].to_i
mod = num_retries % 5
$evm.log(:info, "VRA->num_retries = #{num_retries}, modulo 5 = #{mod}")

status = $evm.inputs['status']
$evm.log(:info, "VRA->notify5 being set to #{$evm.inputs['notify5']}")
if $evm.inputs['notify5'] 
  if mod == 0
	  $evm.log(:info, "VRA->notify5 set (retries = #{num_retries}")
  	  notify = true
  else 
   	  $evm.log(:info, "VRA->notify5 NOT set (retries = #{num_retries}")
  end
else 
  $evm.log(:info, "VRA->notify being set to #{$evm.inputs['notify']}")
  notify = $evm.inputs['notify']  
end
  

$evm.log(:info,"VRA-> status input is [#{status}]")
# Update Status Message
updated_message  = "[#{$evm.root['miq_server'].name}]\n"
#updated_message += "VM [#{prov.get_option(:vm_target_name)}] "
updated_message += "Step [#{$evm.root['ae_state']}]\n "
updated_message += "Status [#{status}]\n "
updated_message += "Message [#{prov.message}]\n "
updated_message += "Current Retry Number [#{$evm.root['ae_state_retries']}]\n" if $evm.root['ae_result'] == 'retry'
prov.miq_request.user_message = updated_message
prov.message = status

if $evm.root['ae_result'] == "error"
  $evm.create_notification(:level => "error", :subject => prov.miq_request, \
                           :message => "VM Provision Error: #{updated_message}")
  $evm.log(:error, "VM Provision Error: #{updated_message}")
elsif notify 
  $evm.create_notification(:level => "info", :subject => prov.miq_request, \
                           :message => "VM Provision Status: #{updated_message}")
  $evm.log(:info, "VM Provision Status: #{updated_message}")
else 
    $evm.log(:info, "VM Provision Status: #{updated_message}")
  
end
