#
# Description: <Method description here>
#
require 'vra'
require 'json'
$evm.log(:info, "VRA->****************************************************************************************")
$evm.log(:info, "VRA->Starting VRA Get Blueprints")

vrahost = "https://"+ $evm.object['vra_host']

$evm.log(:info, "VRA->Vrahost is #{vrahost}")
$evm.log(:info,vrahost)
vra = Vra::Client.new(username: $evm.object['vra_username'],password: $evm.object['vra_userpassword'],tenant: $evm.object['vra_tenant'], base_url: vrahost , verify_ssl: false)
$evm.log(:info, "VRA->Got VRA Connection")

dialog_field = $evm.object

# sort_by: value / description / none
dialog_field["sort_by"] = "value"

# sort_order: ascending / descending
dialog_field["sort_order"] = "ascending"

# data_type: string / integer
dialog_field["data_type"] = "string"

# required: true / false
dialog_field["required"] = "true"
tmphash = Hash.new
vra.catalog.entitled_items.each { |bp| tmphash[bp.name]=bp.id}


#dialog_field["values"] = {1 => "one", 2 => "two", 10 => "ten", 50 => "fifty"}
dialog_field["values"] = tmphash
#dialog_field["default_value"] = 2





catalogitemid = $evm.object['catalogitemid']
catalog_request = vra.catalog.request(catalogitemid, cpus: num_cpu, memory: mem_mb, requested_for: 'cars@ad.lab.lostroncos.net')

catalog_request.subtenant_id = $evm.object['subtenantid']

request = catalog_request.submit
$evm.log(:info,"VRA->Request ID is #{request.id}")
$evm.log(:info,"VRA->Request Status is #{request.status}")

if request.status == "IN_PROGRESS" || request.status == "PENDING_PRE_APPROVAL"
	$evm.log(:info,"VRA->Trying to set state var with value of #{request.id}")
 	$evm.set_state_var("request_id",request.id)
  	$evm.log(:info,"VRA->Trying to set Schema value for requestid")
	$evm.object['requestid'] =  request.id
	$evm.log(:info,"VRA->Set Schema value for requestid")
	exit MIQ_OK
else 
  	exit MIQ_ERROR
end
