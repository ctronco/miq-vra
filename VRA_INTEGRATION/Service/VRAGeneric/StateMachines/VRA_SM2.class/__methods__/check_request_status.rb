require 'vra'
require 'json'

$evm.log(:info,"VRA->starting check_request_status")

vrahost = "https://"+ $evm.object['vra_host']
vra = Vra::Client.new(username: $evm.object['vra_username'],password: $evm.object['vra_userpassword'],tenant: $evm.object['vra_tenant'], base_url: vrahost , verify_ssl: false)
$evm.log(:info,"VRA->got VRA Connection to #{vrahost}")

#if  $evm.object['requestid'].to_s.empty? 
#	$evm.log(:warn,"VRA->requestid schema variable is not present")  
#else 
#  tmpreqid = $evm.object['requestid']
#  $evm.log(:info,"VRA->requestid schema variable is #{tmpreqid}")  
#end
if $evm.state_var_exist?("request_id")
  	$evm.log(:info,"VRA->request_id state variable FOUND")  
 request_id = $evm.get_state_var("request_id")
else 
	$evm.log(:info,"VRA->request_id state variable NOT FOUND")  
	$evm.log(:error,"VRA->request_id state variable NOT FOUND")  
    exit MIQ_STOP
end
$evm.log(:info,"VRA->request_id = [ #{request_id} ]")



request_to_check = vra.requests.by_id(request_id)

#  $evm['ae_retry_interval'] = 1.minute

if request_to_check.status == "IN_PROGRESS" || request_to_check.status == "PENDING_PRE_APPROVAL"
	$evm.log(:info,"VRA->Status of Request #{request_id} is #{request_to_check.status}")  
  	$evm.log(:info,"VRA->Setting Result to Retry")  
	$evm.root['ae_result'] = 'retry'
    $evm.root['ae_retry_interval'] = '1.minute'
    exit MIQ_OK
elsif request_to_check.status.eql? "SUCCESSFUL"
  	$evm.log(:info,"VRA->Status of Request #{request_id} is SUCCESSFUL")  
    $evm.root['ae_result'] = 'ok'
    exit MIQ_OK
else 
   	$evm.log(:info,"VRA->Status of Request #{request_id} is unexpected = #{request_to_check.status}")  
    $evm.root['ae_result'] = 'error'
    $evm.root['ae_reason'] = "Status of Request #{request_id} is unexpected =  #{request_to_check.status}"
    exit MIQ_STOP
end
  
