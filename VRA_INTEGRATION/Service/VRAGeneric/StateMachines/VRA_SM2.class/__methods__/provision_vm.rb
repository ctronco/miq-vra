#
# Description: <Method description here>
#
require 'vra'
require 'json'
$evm.log(:info, "VRA->Starting VRA provision")

$evm.log(:info,"VRA->checking dialog values")
num_cpu = $evm.root['dialog_vra_vm_cpu']
$evm.log(:info,"VRA->Got #{num_cpu} for CPUs from dialog")
mem_gb = $evm.root['dialog_vra_vm_mem']
mem_mb = mem_gb * 1024
$evm.log(:info,"VRA->Got #{mem_gb} for memory from dialog")

bp_catalog_id = $evm.root['dialog_dropdown_bp_list']
$evm.log(:info, "VRA->got #{bp_catalog_id} for Dropdownlist from dialog")

vrahost = "https://"+ $evm.object['vra_host']
$evm.log(:info,"VRA->****************************************************************************************")
$evm.log(:info,"VRA->Vrahost is #{vrahost}")
$evm.log(:info,vrahost)
vra = Vra::Client.new(username: $evm.object['vra_username'],password: $evm.object['vra_userpassword'],tenant: $evm.object['vra_tenant'], base_url: vrahost , verify_ssl: false)

catalogitemid = $evm.root['dialog_dropdown_bp_list']
catalog_request = vra.catalog.request(catalogitemid, cpus: num_cpu, memory: mem_mb, requested_for: 'cars@ad.lab.lostroncos.net')

catalog_request.subtenant_id = $evm.object['subtenantid']

request = catalog_request.submit
$evm.log(:info,"VRA->Request ID is #{request.id}")
$evm.log(:info,"VRA->Request Status is #{request.status}")

if request.status == "IN_PROGRESS" || request.status == "PENDING_PRE_APPROVAL"
	$evm.log(:info,"VRA->Trying to set state var with value of #{request.id}")
 	$evm.set_state_var("request_id",request.id)
  	$evm.log(:info,"VRA->Trying to set Schema value for requestid")
	$evm.object['requestid'] =  request.id
	$evm.log(:info,"VRA->Set Schema value for requestid")
	exit MIQ_OK
else 
  	exit MIQ_ERROR
end
