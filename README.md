VRA ManageIQ Integration
========================
Initial Pass at showing integration between VRA and ManageIQ

To set up: 

Install the vra gem

`gem install vmware-vra`

Next install the RedHat Consulting rake scripts
`cd /tmp`

`if [ -d cfme-rhconsulting-scripts-master ] ; then
    rm -fR /tmp/cfme-rhconsulting-scripts-master`
fi`

`wget -O cfme-rhconsulting-scripts.zip https://github.com/rhtconsulting/cfme-rhconsulting-scripts/archive/master.zip
unzip cfme-rhconsulting-scripts.zip
cd cfme-rhconsulting-scripts-master
make install`

then Import this repo's contet

`miqimport service_dialogs
miqimport service_catalog
miqimport domain VRA_INTEGRATION ./miq_ae_datastore`
